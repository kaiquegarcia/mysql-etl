;(function(w,d,$,undefined) {
    let wait = (ms) => {
        return new Promise((resolve) => {
            w.setTimeout(() => {
                resolve();
            }, ms);
        });
    };
    w.ETL = {};

    // WordPress => Versa
    let waitingTime = 400;
    w.ETL['WP2VERSA'] = (selectors, modes) => {
        $('[name="where"]').val(
            '`post_type`=\'post\' AND ' +
            '`post_status` IN (\'draft\', \'publish\') AND ' +
            '`post_title`!=\'\' AND ' +
            '`post_name`!=\'\''
        );
        return [
            (row) => {
                row.find(selectors.modeSelector)
                    .val(modes.values)
                    .trigger('change');
                row.find(selectors.outputName)
                    .val('thumb');
                row.find(selectors.inputValues)
                    .val('../wp-content/uploads/2017/08/Imagem2.jpg');
                return wait(waitingTime);
            },
            (row) => {
                row.find(selectors.modeSelector).val(modes.values).trigger('change');
                row.find(selectors.outputName).val('categoryID');
                row.find(selectors.inputValues).val('7');
                return wait(waitingTime);
            },
            (row) => {
                row.find(selectors.modeSelector).val(modes.column).trigger('change');
                row.find(selectors.inputName).val('post_status');
                row.find(selectors.inputValuesTranslationsChecker).prop('checked', true).trigger('change');
                row.find(selectors.inputValuesTranslations).val("draft=0\r\npublish=1");
                row.find(selectors.outputName).val('status');
                return wait(waitingTime);
            },
            (row) => {
                row.find(selectors.modeSelector).val(modes.column).trigger('change');
                row.find(selectors.inputName).val('post_title');
                row.find(selectors.outputName).val('title');
                return wait(waitingTime);
            },
            (row) => {
                row.find(selectors.modeSelector).val(modes.column).trigger('change');
                row.find(selectors.inputName).val('post_content');
                row.find(selectors.outputName).val('content1');
                return wait(waitingTime);
            },
            (row) => {
                row.find(selectors.modeSelector).val(modes.column).trigger('change');
                row.find(selectors.inputName).val('post_date');
                row.find(selectors.outputName).val('init_date');
                return wait(waitingTime);
            },
            (row) => {
                row.find(selectors.modeSelector).val(modes.column).trigger('change');
                row.find(selectors.inputName).val('post_date');
                row.find(selectors.outputName).val('post_date');
                return wait(waitingTime);
            },
            (row) => {
                row.find(selectors.modeSelector).val(modes.values).trigger('change');
                let date = new DateTime();
                row.find(selectors.inputValues).val(date.getISODateTime());
                row.find(selectors.outputName).val('status_date');
                return wait(waitingTime);
            },
            (row) => {
                row.find(selectors.modeSelector).val(modes.values).trigger('change');
                row.find(selectors.inputValues).val('0000-00-00 00:00:00');
                row.find(selectors.outputName).val('end_date');
                return wait(waitingTime);
            },
            (row) => {
                row.find(selectors.modeSelector).val(modes.column).trigger('change');
                row.find(selectors.inputName).val('post_name');
                row.find(selectors.outputName).val('URL');
                return wait(waitingTime);
            }
        ];
    };
    w.ETL['DEPMARIA'] = (selectors, modes) => {
        return [
            (row) => {
                row.find(selectors.modeSelector).val(modes.column).trigger('change');
                row.find(selectors.inputName).val('foto');
                row.find(selectors.inputValuesPregTranslationsChecker).prop('checked', true).trigger('change');
                row.find(selectors.inputValuesPregTranslations).val('/^(\\s{1,4})?(fotos_)/=../$2', '');
                row.find(selectors.inputValuesTranslationsChecker).prop('checked', true).trigger('change');
                row.find(selectors.inputValuesTranslations).val('=uploads/publicacoes/d3647b8a6d83736063f08f9d11402a96.jpg\r\nflt=uploads/publicacoes/d3647b8a6d83736063f08f9d11402a96.jpg')
                row.find(selectors.outputName).val('thumb');
                return wait(waitingTime);
            },
            (row) => {
                row.find(selectors.modeSelector).val(modes.values).trigger('change');
                row.find(selectors.outputName).val('categoryID');
                row.find(selectors.inputValues).val('7');
                return wait(waitingTime);
            },
            (row) => {
                row.find(selectors.modeSelector).val(modes.values).trigger('change');
                row.find(selectors.inputValues).val('1')
                row.find(selectors.outputName).val('status');
                return wait(waitingTime);
            },
            (row) => {
                row.find(selectors.modeSelector).val(modes.column).trigger('change');
                row.find(selectors.inputName).val('titulo');
                row.find(selectors.outputName).val('title');
                return wait(waitingTime);
            },
            (row) => {
                row.find(selectors.modeSelector).val(modes.column).trigger('change');
                row.find(selectors.inputName).val('titulo');
                row.find(selectors.outputName).val('URL');
                return wait(waitingTime);
            },
            (row) => {
                row.find(selectors.modeSelector).val(modes.column).trigger('change');
                row.find(selectors.inputName).val('texto');
                row.find(selectors.outputName).val('content1');
                return wait(waitingTime);
            },
            (row) => {
                row.find(selectors.modeSelector).val(modes.column).trigger('change');
                row.find(selectors.inputName).val('data, ,hora');
                row.find(selectors.inputValuesPregTranslationsChecker).prop('checked', true).trigger('change');
                let regexps = [
                    '/[\\\/\\\\]/=.',
                    '/^(\\d{0,1})(\\d{2}).(\\d{2}).(\\d{4}) (\\d{2}):(\\d{2})$/=$4-$3-$2 $5:$6:00',
                    '/^(\\d{2}).(\\d{1})(\\d{1}).(\\d{1}).(\\d{4}) (\\d{2}):(\\d{2})$/=$5-$2$4-$1 $6:$7:00',
                    '/^(\\d{0,1})(\\d{2}).(\\d{2}).(\\d{2}) (\\d{2}):(\\d{2})$/=20$4-$3-$2 $5:$6:00',
                    '/^[\\.]?(\\d{2}).(\\d{4}) (\\d{2}):(\\d{2})$/=$2-$1-01 $3:$4:00',
                    '/^(\\d{2}).(\\d{2}) (\\d{2}):(\\d{2})$/=20$2-$1-01 $3:$4:00',
                    '/^(\\d{4})-52-(\\d{2}) (\\d{2}):(\\d{2}):(\\d{2})$/=$1-12-$2 $3:$4:$5',
                ];
                row.find(selectors.inputValuesPregTranslations).val(
                    regexps.join('\r\n')
                );
                row.find(selectors.outputName).val('init_date');
                return wait(waitingTime);
            },
            (row) => {
                row.find(selectors.modeSelector).val(modes.values).trigger('change');
                row.find(selectors.inputValues).val('0000-00-00 00:00:00');
                row.find(selectors.outputName).val('end_date');
                return wait(waitingTime);
            },
            (row) => {
                row.find(selectors.modeSelector).val(modes.values).trigger('change');
                let date = new DateTime();
                row.find(selectors.inputValues).val(date.getISODateTime());
                row.find(selectors.outputName).val('status_date');
                return wait(waitingTime);
            },
            (row) => {
                row.find(selectors.modeSelector).val(modes.column).trigger('change');
                row.find(selectors.inputName).val('data');
                row.find(selectors.inputValuesPregTranslationsChecker).prop('checked', true).trigger('change');
                let regexps = [
                    '/[\\\/\\\\]/=.',
                    '/^(\\d{0,1})(\\d{2}).(\\d{2}).(\\d{4})$/=$4-$3-$2',
                    '/^(\\d{2}).(\\d{1})(\\d{1}).(\\d{1}).(\\d{4})$/=$5-$2$4-$1',
                    '/^(\\d{0,1})(\\d{2}).(\\d{2}).(\\d{2})$/=20$4-$3-$2',
                    '/^[\\.]?(\\d{2}).(\\d{4})$/=$2-$1-01',
                    '/^(\\d{2}).(\\d{2})$/=20$2-$1-01',
                    '/^(\\d{4})-52-(\\d{2})$/=$1-12-$2',
                ];
                row.find(selectors.inputValuesPregTranslations).val(
                    regexps.join('\r\n')
                );
                row.find(selectors.outputName).val('post_date');
                return wait(waitingTime);
            }
        ];
    };
})(window, document, jQuery);