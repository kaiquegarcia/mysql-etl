let getResume = () => {
    return new Promise((resolve, reject) => {
        $.ajax({
            type: "POST",
            url: "responses/resume.php",
            dataType: "json",
            success: resolve,
            error: reject
        });
    });
};
let doResume = (datetime, IP) => {
    let recall = () => {doResume(datetime, IP);};
    getResume().then(function(response) {
        datetime.html(response.datetime);
        IP.html(response.IP);
        window.setTimeout(recall, 1000);
    }, () => {
        window.setTimeout(recall, 200);
    });
};

doResume($('#datetime'), $('#IP'));