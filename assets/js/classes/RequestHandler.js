class RequestHandler {
    constructor(url, data) {
        this.url = url;
        this.data = data;
        this.promise = null;
    }
    callback(resolve, reject) {
        $.ajax({
            type: "POST",
            url: this.url,
            data: this.data,
            dataType: "json",
            success: (response) => {
                if(response.status === 1) {
                    resolve(response);
                } else {
                    console.log(response.data, response.trace);
                    if(response.message) {
                        reject(response.message);
                    } else {
                        reject("Ocorreu uma falha inesperada.");
                    }
                }
            },
            error: (response) => {
                console.log(response);
                reject(response.responseText);
            }
        });
    }
    request()
    {
        this.promise = new Promise(Utils.handlerGenerator(this, "callback"));
        return this;
    }
    then(...args)
    {
        if(this.promise !== null) {
            this.promise.then(...args);
        }
        return this;
    }
    onSuccess(func)
    {
        if(this.promise !== null) {
            this.promise.then(func);
        }
        return this;
    }
    onError(func)
    {
        if(this.promise !== null) {
            this.promise.catch(func);
        }
        return this;
    }
    static getCSV(url, filters) {
        filters = typeof(filters) == "string" ? "?" + filters : "";
        let iframe = document.createElement('iframe');
        iframe.style.display = 'none';
        iframe.onload = function() {
            if(this.contentWindow.document.body.innerHTML !== '') {
                window.alert(this.contentWindow.document.body.innerHTML);
            }
        };
        iframe.src = url + filters;
        $('body').append(iframe);
    }
}