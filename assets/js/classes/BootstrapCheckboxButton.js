class BootstrapCheckboxButton {
    static colorClasses()
    {
        return [
            "secondary",
            "primary",
            "success",
            "info",
            "warning",
            "danger",
        ];
    }
    constructor(element)
    {
        this.element = element;
        this.setColorClass();
        this.setCheckbox();
    }
    setColorClass()
    {
        let colorClass = this.element.attr('data-colorclass');
        let colorClasses = BootstrapCheckboxButton.colorClasses();
        if(colorClasses.indexOf(colorClass) === -1) {
            colorClass = colorClasses[0];
        }
        this.onClass = 'btn-' + colorClass;
        this.offClass = 'btn-outline-' + colorClass;
        this.element
            .addClass('btn btn-sm ' + this.offClass)
            .removeClass('checkbox-button')
            .css({
                'cursor': 'pointer'
            });
    }
    setCheckbox()
    {
        this.element.append('<input type="checkbox" style="visibility: hidden; height: 0; width: 0;" value="1" />')
        this.checkbox = this.element.find('input[type="checkbox"]');
        this.checkbox
            .prop('checked', this.element.attr('data-state') === "on");
        let inputName = this.element.attr('data-inputname'),
            inputClass = this.element.attr('data-inputclass');
        if(inputName) {
            this.checkbox.attr('name', inputName);
        }
        if(inputClass) {
            this.checkbox.addClass(inputClass);
        }
        let $this = this;
        this.checkbox.on('change', function() {
            $this.setCheckedStateFromInput();
        }).trigger('change');
    }
    setCheckedStateIf(condition, isOnChangeEvent) {
        if(condition) {
            this.setChecked(isOnChangeEvent);
        } else {
            this.setUnchecked(isOnChangeEvent);
        }
    }
    setChecked(isOnChangeEvent)
    {
        if(isOnChangeEvent === undefined) {
            isOnChangeEvent = false;
        }
        this.element
            .removeClass(this.offClass)
            .addClass(this.onClass)
            .attr('data-state', 'on');
        if(!isOnChangeEvent) {
            this.checkbox.prop('checked', true);
        }
    }
    setUnchecked(isOnChangeEvent)
    {
        if(isOnChangeEvent === undefined) {
            isOnChangeEvent = false;
        }
        this.element
            .removeClass(this.onClass)
            .addClass(this.offClass)
            .attr('data-state', 'off');
        if(!isOnChangeEvent) {
            this.checkbox.prop('checked', false);
        }
    }
    setCheckedStateFromInput()
    {
        this.setCheckedStateIf(this.checkbox.is(':checked'), true);
    }
    toggle()
    {
        this.setCheckedStateIf(!this.checkbox.is(':checked'), false);
    }
}