class DateTime {
    constructor(...JavascriptDateInput) {
        let date = new Date(...JavascriptDateInput);
        this.date = {};
        this.date['Y'] = Utils.setNumberDigits(date.getFullYear(), 4);
        this.date['m'] = Utils.setNumberDigits(date.getMonth()+1, 2);
        this.date['d'] = Utils.setNumberDigits(date.getDate(), 2);
        this.date['H'] = Utils.setNumberDigits(date.getHours(), 2);
        this.date['i'] = Utils.setNumberDigits(date.getMinutes(), 2);
        this.date['s'] = Utils.setNumberDigits(date.getSeconds(), 2);
    }
    getValues(...vars)
    {
        let result = [];
        for(let i in vars) {
            let v = vars[i];
            if(typeof(this.date[v]) != "undefined") {
                result.push(this.date[v]);
            }
        }
        return result;
    }
    getTime()
    {
        return this.getValues('H', 'i', 's').join(':');
    }
    getISODate()
    {
        return this.getValues('Y', 'm', 'd').join('-');
    }
    getISODateTime()
    {
        return this.getISODate() + ' ' + this.getTime();
    }
    getYear()
    {
        return this.date['Y'];
    }
    getMonth()
    {
        return this.date['m'];
    }
    getDay()
    {
        return this.date['d'];
    }
    getHour()
    {
        return this.date['H'];
    }
    getMinute()
    {
        return this.date['i'];
    }
    getSecond()
    {
        return this.date['s'];
    }
}