class AutoCopy {
    constructor(element) {
        this.element = element;
        this.initialize();
    }
    initialize() {
        this.element.css({
            cursor: 'pointer'
        })
        this.shadow = $('<textarea class="autocopy-shadow" style="position: absolute; top: -100px; left: -100px; max-width: 100px; max-height: 100px; overflow: hidden;"></textarea>');
        $('body').append(this.shadow);
        this.shadow = $('.autocopy-shadow:last-child').get(0);
        let $this = this;
        this.element.click((event) => {
            $this.execute(event);
        });
    }
    execute(event) {
        event.preventDefault();
        this.shadow.innerHTML = this.element.text();
        this.shadow.select();
        document.execCommand('copy');
        window.alert('Copiado!');
    }
}