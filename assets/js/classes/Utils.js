class Utils {
    static valueFrom(haystack, ...deeperIndexes)
    {
        if(deeperIndexes.length === 0) {
            return haystack;
        }
        for(let i in deeperIndexes) {
            let index = deeperIndexes[i];
            if("undefined" != typeof(haystack[index])) {
                haystack = haystack[index];
            } else {
                haystack = null;
                break;
            }
        }
        return haystack;
    }
    static valuesFrom(haystack, ...multipleIndexes)
    {
        let output = {length: 0};
        if(multipleIndexes.length === 0) {
            return output;
        }
        for(let i in multipleIndexes) {
            let index = multipleIndexes[i];
            output[index] = this.valueFrom(haystack, index);
            output.length++;
        }
        return output;
    }
    static requiredValuesFrom(haystack, ...multipleIndexes)
    {
        let output = {length: 0};
        if(multipleIndexes.length === 0) {
            return output;
        }
        for(let i in multipleIndexes) {
            let index = multipleIndexes[i];
            output[index] = this.valueFrom(haystack, index);
            if(output[index] === null) {
                throw new Error("Comando inválido.");
            }
            output.length++;
        }
        return output;
    }
    static handlerGenerator(object, func, ...args)
    {
        let useInput = args.length === 0;
        if("function" === typeof(object[func])) {
            return (...input) => {
                if(useInput) {
                    object[func](...input);
                } else {
                    object[func](...args);
                }
            };
        } else {
            return () => {};
        }
    }
    static setNumberDigits(number, digits)
    {
        number = number.toString();
        if(number.length < digits) {
            return "0".repeat(digits - number.length) + number;
        }
        return number;
    }
}