(function(w,d,$,undefined) {
    let sending = false;
    let form = $('form'),
        button = form.find('button[type="submit"]').get(0);
    let ready = false;
    let columnRows = $('.column-rows');
    let columnRow = "",
        columnAdd = (setLine) => {},
        resetColumns = () => {
            columnRows.html('');
            columnAdd(false);
        },
        setColumnsPresets = (columns, current) => {
            if(current === undefined) {
                current = 0;
                resetColumns();
            }
            if(typeof(columns[current]) !== "undefined") {
                let wait = columns[current]($('.ETL-mode-selector[data-last="1"]').parents('.column-row'));
                wait.then(() => {
                    setColumnsPresets(columns, current+1);
                });
            }
        };
    let catcher = new Promise((resolve, reject) => {
        $.get("views/column-row.html", function(template) {
            columnRow = template;
            resolve();
        });
    });
    catcher.then(function() {
        ready = true;
        columnAdd = (setLine) => {
            if(setLine) {
                columnRows.append('<hr/>');
            }
            columnRows.append(columnRow);
            $('.column-row:last-child').find('.checkbox-button').each(function() {
                new BootstrapCheckboxButton($(this));
            });
        };
        $(d).on('change', '.ETL-mode-selector', function() {
            let isLast = $(this).attr('data-last') === "1";
            let mode = $(this).val();
            let parent = $(this).parents('.column-row');
            parent.find('.ETL-mode-input[data-mode!="' + mode + '"]')
                .hide().find('input,textarea').val('');
            parent.find('.ETL-mode-input[data-mode="' + mode + '"]').show();
            if(isLast) {
                $(this).attr('data-last', "0");
                columnAdd(true);
            }
        });
        let btnInfo = {
            checked: 'btn-info',
            unchecked: 'btn-outline-info'
        }, btnSuccess = {
            checked: 'btn-success',
            unchecked: 'btn-outline-success'
        };
        $(d).on('change', '.ETL-translations-checker', function() {
            let parent = $(this).parents('.column-row');
            let isChecked = $(this).is(':checked'), event = 'show';
            let button = $(this).parents('.btn');
            if(isChecked) {
                button.removeClass(btnInfo.unchecked).addClass(btnInfo.checked);
            } else {
                button.removeClass(btnInfo.checked).addClass(btnInfo.unchecked);
                event = 'hide';
            }
            parent.find('.ETL-translations')[event]();
        });
        $(d).on('change', '.ETL-preg-translations-checker', function() {
            let parent = $(this).parents('.column-row');
            let isChecked = $(this).is(':checked'), event = 'show';
            let button = $(this).parents('.btn');
            if(isChecked) {
                button.removeClass(btnSuccess.unchecked).addClass(btnSuccess.checked);
            } else {
                button.removeClass(btnSuccess.checked).addClass(btnSuccess.unchecked);
                event = 'hide';
            }
            parent.find('.ETL-preg-translations')[event]();
        });
        resetColumns();
    });

    form.submit(function(e) {
        e.preventDefault();
        if(!ready) {
            w.alert("Espera! O formulário está sendo preparado...");
            return;
        }
        if(sending) {
            return;
        }
        sending = true;
        button.disabled = true;
        let handler = new RequestHandler(
            'responses/home.php',
            $(this).serialize()
        );
        handler.request().then(
            (response) => {
                alert(response.message);
                sending = false;
                button.disabled = false;
            },
            (error) => {
                alert(error);
                sending = false;
                button.disabled = false;
            }
        )
    });
    $('.checkbox-button').each(function() {
        new BootstrapCheckboxButton($(this));
    });
    $('.download-csv').click(function(e) {
        e.preventDefault();
        if(!ready) {
            w.alert("Espera! O formulário está sendo preparado...");
            return;
        }
        RequestHandler.getCSV('responses/export.php', form.serialize());
    });
    $('.test-input').click(function(e) {
        e.preventDefault();
        if(!ready) {
            w.alert("Espera! O formulário está sendo preparado...");
            return;
        }
        let handler = new RequestHandler(
            'responses/test-input.php',
            form.serialize()
        );
        handler.request().then(
            (response) => {
                alert(response.message);
            },
            alert
        );
    });
    $('.test-output').click(function(e) {
        e.preventDefault();
        if(!ready) {
            w.alert("Espera! O formulário está sendo preparado...");
            return;
        }
        let handler = new RequestHandler(
            'responses/test-output.php',
            form.serialize()
        );
        handler.request().then(
            (response) => {
                alert(response.message);
            },
            alert
        );
    });
    $('.reset-columns').click(function(e) {
        e.preventDefault();
        resetColumns();
    });
    $('.autocopy').each(function() {
        new AutoCopy($(this));
    });
    let selectors = {
        modeSelector: '.ETL-mode-selector',
        inputName: '[name="ETL[inputNames][]"]',
        inputValues: '[name="ETL[inputValues][]"]',
        inputValuesTranslations: '[name="ETL[inputValuesTranslations][]"]',
        inputValuesTranslationsChecker: '.ETL-translations-checker',
        inputValuesPregTranslations: '[name="ETL[inputValuesPregTranslations][]"]',
        inputValuesPregTranslationsChecker: '.ETL-preg-translations-checker',
        outputName: '[name="ETL[outputNames][]"]'
    };
    let modes = {
        column: 'inputNames',
        values: 'inputValues'
    }
    if(typeof(w.ETL['WP2VERSA']) !== "undefined") {
        $('.wp-2-versa').click(function (e) {
            e.preventDefault();
            if (!ready) {
                w.alert("Espera! O formulário está sendo preparado...");
                return;
            }
            setColumnsPresets(w.ETL['WP2VERSA'](selectors, modes));
        });
    }
    if(typeof(w.ETL['DEPMARIA']) !== "undefined") {
        $('.depmaria').click(function (e) {
            e.preventDefault();
            if (!ready) {
                w.alert("Espera! O formulário está sendo preparado...");
                return;
            }
            setColumnsPresets(w.ETL['DEPMARIA'](selectors, modes));
        });
    }
})(window, document, jQuery);