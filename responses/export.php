<?php

require_once("../autoload.php");


try {
    $required = \ETL\Handlers\RequestHandler::requireGET("input", "ETL", "where");
    $inputDB = \ETL\Handlers\Utils::requiredValuesFrom(
        $required["input"],
        "host", "user", "password", "schema", "database"
    );

    $ETL = \ETL\Handlers\Utils::requiredValuesFrom(
        $required['ETL'],
        "inputNames", "inputValues", "inputValuesTranslations", "inputValuesPregTranslations", "outputNames"
    );

    $inputRepository = new \ETL\Database\Repository($inputDB);

    $page = 0;
    $adapter = new \ETL\Origin\Adapter($ETL);
    $output = [];
    $where = "";
    if($required['where'] != "") {
        $where = " WHERE {$required['where']}";
    }
    while (true) {
        $list = $inputRepository->getAll(
            $where,
            $page
        );
        if (empty($list) || !is_array($list)) {
            break;
        }
        foreach ($list as $input) {
            $output[] = $adapter->getOutput($input);
        }
        $page++;
    }
    if(empty($output)) {
        throw new Exception("Não há dados para exibir.");
    }
    \ETL\Handlers\Utils::toCSV($output, "result");
} catch(Exception $e) {
    \ETL\Handlers\Result::error("Ocorreu um erro. \r\n{$e->getMessage()}", $e->getTrace());
}