<?php

use ETL\Database\Connector;
use ETL\Exceptions\DatabaseException;
use ETL\Exceptions\InvalidCommandException;
use ETL\Handlers\RequestHandler;
use ETL\Handlers\Result;
use ETL\Handlers\Utils;

require_once("../autoload.php");

try {
    if(!isset($inputVar)) {
        throw new InvalidCommandException();
    }
    $required = RequestHandler::requirePOST($inputVar);
    $input = Utils::requiredValuesFrom(
        $required[$inputVar],
        "host", "user", "password", "schema", "database"
    );
    Connector::test($input);
} catch (DatabaseException $e) {
    Result::error("Ocorreu uma falha ao gerar essa conexão: {$e->getMessage()}", $e->getTrace());
} catch (InvalidCommandException $e) {
    Result::error("Comando inválido.", $e->getTrace());
} catch(Exception $e) {
    Result::error("Ocorreu uma falha inesperada. {$e->getMessage()}", $e->getTrace());
}