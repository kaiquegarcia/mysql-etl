<?php

$datetime = date('d/m/Y H:i:s');
$HOST = gethostname();
$IP = gethostbyname($HOST);

$output = [
    "datetime" => $datetime,
    "IP" => $IP,
];
echo json_encode($output);