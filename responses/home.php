<?php

use ETL\Database\Repository;
use ETL\Handlers\RequestHandler;
use ETL\Handlers\Result;
use ETL\Handlers\Utils;
use ETL\Origin\Adapter;

require_once("../autoload.php");


$count = 0;
try {
    $required = RequestHandler::requirePOST("input", "output", "ETL", "where");
    $inputDB = Utils::requiredValuesFrom(
        $required["input"],
        "host", "user", "password", "schema", "database"
    );
    $outputDB = Utils::requiredValuesFrom(
        $required["output"],
        "host", "user", "password", "schema", "database"
    );

    $ETL = Utils::requiredValuesFrom(
        $required['ETL'],
        "inputNames", "inputValues", "inputValuesTranslations", "inputValuesPregTranslations", "outputNames"
    );

    $inputRepository = new Repository($inputDB);
    $outputRepository = new Repository($outputDB);

    $outputRepository->resetSQLMode();

    $TRUNCATE = Utils::valueFrom(
        $required['ETL'],
        "TRUNCATE"
    );

    if($TRUNCATE) {
        $outputRepository->truncate();
    }

    $page = 0;
    $adapter = new Adapter($ETL);
    $where = "";
    if($required['where'] != "") {
        $where = " WHERE {$required['where']}";
    }
    while (true) {
        $list = $inputRepository->getAll(
            $where,
            $page
        );
        if (empty($list) || !is_array($list)) {
            break;
        }
        foreach ($list as $input) {
            $adapter->prepareInsert(
                $outputRepository,
                $input
            );
        }
        $page++;
    }
    $count = $outputRepository->executeSingleInsert();
} catch(Exception $e) {
    Result::error("Ocorreu um erro. \r\n{$e->getMessage()}", $e->getTrace());
}
Result::success("Transferência concluída com sucesso. $count registros transferidos.");