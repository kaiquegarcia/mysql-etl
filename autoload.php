<?php

ini_set("default_charset", "UTF-8");
define ('BASE', dirname(__FILE__).DIRECTORY_SEPARATOR);
define('CLASS_PATH', BASE.'classes');
function pullClasses($classname)
{
    $filename = str_replace(['ETL', '\\'], ['', DIRECTORY_SEPARATOR], $classname);
    $absolute = CLASS_PATH.$filename.".php";
    if(file_exists($absolute)) {
        include_once($absolute);
    }
}
spl_autoload_register("pullClasses");