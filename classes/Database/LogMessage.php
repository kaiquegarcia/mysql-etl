<?php

namespace ETL\Database;

class LogMessage {
    private $message;
    private $timestamp;

    public function __construct($message)
    {
        $this->message = $message;
        if($message != "") {
            $this->timestamp = date("Y-m-d H:i:s");
        }
    }

    public function getLog()
    {
        return "{$this->timestamp} | {$this->message}\r\n";
    }
}