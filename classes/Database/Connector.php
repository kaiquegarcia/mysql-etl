<?php

namespace ETL\Database;

use ETL\Exceptions\DatabaseException;
use ETL\Exceptions\InvalidCommandException;
use ETL\Handlers\Result;
use ETL\Handlers\Utils;

class Connector
{
    private $connection,
        $lastQuery,
        $schema,
        $lastAffectedRows;

    /**
     * Connector constructor.
     * @param array $input
     * @throws DatabaseException
     * @throws InvalidCommandException
     */
    public function __construct(array $input = [])
    {
        $config = array_values(
            Utils::requiredValuesFrom($input, "host", "user", "password", "schema")
        );
        if (in_array(null, $config)) {
            throw new DatabaseException("Null parameters");
        }
        $this->schema = $config[3];
        $this->connection = new \mysqli(...$config);
        if ($this->connection->errno) {
            throw new DatabaseException($this->connection->error);
        }
        $this->connection->set_charset("utf-8");
    }

    public function getSchema()
    {
        return $this->schema;
    }

    public function getConnection()
    {
        return $this->connection;
    }

    public function getLastQuery()
    {
        return $this->lastQuery;
    }

    public function getAffectedRows()
    {
        return $this->lastAffectedRows;
    }

    private function hasList($query)
    {
        return strpos(strtoupper($query), "SELECT") !== false;
    }

    private function argTypes(array $args)
    {
        $types = "";
        foreach ($args as $arg) {
            if (is_int($arg)) {
                $types .= "i";
            } elseif (is_float($arg)) {
                $types .= "d";
            } else {
                $types .= "s";
            }
        }
        return array_merge([$types], $args);
    }

    /**
     * @param $query
     * @return bool|mixed
     */
    private function singleQuery($query)
    {
        $result = $this->connection->query($query);
        $this->lastAffectedRows = $this->connection->affected_rows;
        return self::hasList($query)
            ? $result->fetch_all(MYSQLI_ASSOC)
            : $this->getAffectedRows() > 0;
    }

    /**
     * @param $query
     * @param $args
     * @return bool|mixed
     * @throws DatabaseException
     */
    private function preparedStatement($query, $args)
    {
        $stmt = $this->connection->prepare($query);
        if($this->connection->errno) {
            throw new DatabaseException($this->connection->error);
        }
        $args = self::argTypes($args);
        $stmt->bind_param(...$args);
        $stmt->execute();
        $this->lastAffectedRows = $this->connection->affected_rows;
        $result = $this->getAffectedRows() > 0;
        if(!$result && $stmt->errno) {
            var_dump($stmt->error);
            exit();
        }
        if (self::hasList($query)) {
            $mysqliResult = $stmt->get_result();
            $result = $mysqliResult->fetch_all(MYSQLI_ASSOC);
        }
        $stmt->close();
        return $result;
    }

    /**
     * @param array $input
     * @return bool|mixed
     * @throws DatabaseException
     */
    public function query(array $input)
    {
        $query = Utils::valueFrom($input, "query");
        $args = Utils::valueFrom($input, "args");
        $this->lastQuery = $input;
        if ($args == null) {
            return self::singleQuery($query);
        }
        return self::preparedStatement($query, $args);
    }

    public static function test(array $input)
    {
        try {
            new self($input);
        } catch (DatabaseException $e) {
            Result::error("Ocorreu uma falha ao gerar essa conexão: {$e->getMessage()}");
        } catch (InvalidCommandException $e) {
            Result::error($e->getMessage());
        }
        Result::success("Conectado com sucesso!");
    }
}