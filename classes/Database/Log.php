<?php

namespace ETL\Database;

class Log {
    const LOGGING = true;

    private $filename;
    private $messages = [];

    public function __construct($filename = "default.log")
    {
        $this->filename = $filename;
    }
    public function __destruct()
    {
        $logs = $this->getLogs();
        if($logs != "") {
            $this->genFile($logs);
        }
    }
    private function genFile($data)
    {
        $rollBack = "..".DIRECTORY_SEPARATOR;
        $dir = __DIR__.DIRECTORY_SEPARATOR.$rollBack.$rollBack."logs".DIRECTORY_SEPARATOR;
        $filepath = $dir.$this->filename;
        if(!is_dir(dirname($filepath))) {
            mkdir(dirname($filepath), 0777, true);
        }
        file_put_contents($filepath, $data, FILE_APPEND);
    }
    public function getLogs()
    {
        if(self::LOGGING && !empty($this->messages)) {
            $output = "";
            foreach($this->messages as $message) {
                $output .= $message->getLog();
            }
            return $output;
        }
        return "";
    }
    private function add($message)
    {
        if(self::LOGGING) {
            $message = new LogMessage($message);
            $this->messages[] = $message;
        }
    }
    public function addInfo($message)
    {
        $this->add("INFO: $message");
    }
    public function addWarning($message)
    {
        $this->add("WARNING: $message");
    }
    public function addError($message)
    {
        $this->add("ERROR: $message");
    }
    public function addJSON($json)
    {
        if(is_array($json)) {
            $json = json_encode($json);
        }
        $this->add("JSON: $json");
    }
    public function addRow()
    {
        $this->add("");
    }
}