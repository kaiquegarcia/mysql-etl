<?php

namespace ETL\Database;

use ETL\Exceptions\DatabaseException;
use ETL\Handlers\Utils;

class Repository
{
    private $table_name,
        $con,
        $log,
        $singleInsert = [],
        $singleAffectedRows = 0,
        $singleQueryCount = 0;

    /**
     * Repository constructor.
     * @param array $dbConfig
     * @throws DatabaseException
     * @throws \ETL\Exceptions\InvalidCommandException
     */
    public function __construct(array $dbConfig = [])
    {
        $this->log = new Log("repository.log");
        $this->log->addInfo("Instancing Repository");
        $this->table_name = Utils::valueFrom($dbConfig, "database");
        $this->con = new Connector($dbConfig);
    }

    /**
     * @param string $where
     * @param int $page
     * @param int $limit
     * @return bool|mixed
     * @throws DatabaseException
     */
    public function getAll($where = "", $page = 0, $limit = 500)
    {
        $page *= $limit;
        return $this->con->query([
            "query" =>
                "SELECT *
                FROM `{$this->table_name}`
                $where
                ORDER BY
                    `ID` ASC
                LIMIT $page,$limit"
        ]);
    }

    /**
     * @param array $keyToValues
     * @return bool|mixed
     * @throws DatabaseException
     */
    public function insert(array $keyToValues)
    {
        $columns = array_keys($keyToValues);

        $insert_columns = "`" . implode("`, `", $columns) . "`";
        $insert_params = substr(str_repeat("?,", count($columns)), 0, -1);
        $input = [
            "query" => "INSERT INTO `{$this->table_name}` ($insert_columns) VALUES ($insert_params)",
            "args" => array_values($keyToValues),
        ];
        return $this->con->query($input);
    }

    /**
     * @param array $keyToValues
     * @throws DatabaseException
     */
    public function prepareSingleInsert(array $keyToValues)
    {
        $this->log->addInfo("Preparing Single Insert");
        if($this->singleQueryCount == 30) {
            $this->log->addInfo("Count of Single Insert Queries achieved. Starting to flush");
            $this->executeSingleInsert();
        }
        $columns = array_keys($keyToValues);
        $insert_params = substr(str_repeat("?,", count($columns)), 0, -1);
        if(empty($this->singleInsert)) {
            $this->log->addInfo("Adding a new singleInsert");
            $insert_columns = "`".implode("`, `", $columns)."`";
            $this->singleInsert = [
                "query" => "INSERT INTO `{$this->table_name}` ($insert_columns) VALUES ($insert_params)",
                "args" => array_values($keyToValues),
            ];
            $this->singleQueryCount = 1;
        } else {
            $this->log->addInfo("One more singleInsert");
            $this->singleInsert['query'].= ", ($insert_params)";
            $this->singleInsert['args'] = array_merge($this->singleInsert['args'], array_values($keyToValues));
            $this->singleQueryCount++;
        }
    }

    /**
     * @return bool|mixed
     * @throws DatabaseException
     */
    public function executeSingleInsert()
    {
        if(empty($this->singleInsert)) {
            $this->log->addInfo("There's no queries to execute");
            return $this->singleAffectedRows;
        }
        $this->con->query($this->singleInsert);
        $affectedRows = $this->con->getAffectedRows();
        $this->log->addInfo("Query executed. $affectedRows affected rows");
        $this->singleAffectedRows += $affectedRows;
        $this->singleInsert = [];
        $this->singleQueryCount = 0;
        return $this->singleAffectedRows;
    }

    /**
     * @param $ID
     * @param array $keyToValues
     * @param string $IdColumn
     * @return bool
     * @throws DatabaseException
     */
    public function update($ID, array $keyToValues, $IdColumn = "ID")
    {
        unset($keyToValues['ID']);

        $columns = array_keys($keyToValues);

        $update_columns = "`" . substr(implode("`=?,", $columns), 0, -1);
        $input = [
            "query" => "UPDATE `{$this->table_name}` SET $update_columns WHERE `$IdColumn`=$ID",
            "args" => array_values($keyToValues),
        ];
        return $this->con->query($input);
    }

    /**
     * @return bool|mixed
     * @throws DatabaseException
     */
    public function truncate()
    {
        $input = [
            "query" => "TRUNCATE `{$this->table_name}`",
            "args" => null,
        ];
        return $this->con->query($input);
    }

    /**
     * @throws DatabaseException
     */
    public function resetSQLMode()
    {
        $this->con->query([
            "query" => "SET sql_mode=''",
        ]);
    }

    /**
     * @throws DatabaseException
     */
    public function forceUtf8()
    {
        $this->con->query([
            "query" => "SET NAMES 'utf8'"
        ]);
        $this->con->query([
            "query" => "SET CHARACTER SET utf8"
        ]);
        $this->con->query([
            "query" => "ALTER TABLE {$this->table_name} CONVERT TO CHARACTER SET utf8 COLLATE utf8_unicode_ci"
        ]);
    }
}