<?php

namespace ETL\Handlers;

class Result
{
    const STATUS_SUCCESS = 1;
    const STATUS_ERROR = 2;

    /**
     * @param $message
     * @param $data
     * @param $status
     */
    private static function result($message, $data, $status, $trace = null)
    {
        header("Content-type: application/json; charset=utf-8");
        $output = [
            "message" => $message,
            "data" => $data,
            "status" => $status,
        ];
        if ($trace != null) {
            $output['trace'] = $trace;
        }
        $output = json_encode($output);
        exit($output);
    }

    /**
     * @param string $message
     * @param null $data
     */
    public static function success($message = "", $data = null)
    {
        self::result($message, $data, self::STATUS_SUCCESS);
    }

    /**
     * @param string $message
     * @param null $data
     */
    public static function error($message = "", $data = null)
    {
        self::result($message, $data, self::STATUS_ERROR, debug_backtrace());
    }
}