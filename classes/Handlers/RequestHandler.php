<?php

namespace ETL\Handlers;

use ETL\Exceptions\InvalidCommandException;
use ETL\Exceptions\SessionWontStartException;

class RequestHandler
{
    /**
     * @param mixed ...$deeperIndexes
     * @return array|mixed|null
     */
    public static function GET(...$deeperIndexes)
    {
        return Utils::valueFrom($_GET, ...$deeperIndexes);
    }

    /**
     * @param mixed ...$deeperIndexes
     * @return array|mixed|null
     */
    public static function POST(...$deeperIndexes)
    {
        return Utils::valueFrom($_POST, ...$deeperIndexes);
    }

    /**
     * @throws SessionWontStartException
     */
    private static function startSession()
    {
        if (!isset($_SESSION)) {
            $start = session_start();
            if (!$start) {
                throw new SessionWontStartException();
            }
        }
    }

    /**
     * @param mixed ...$deeperIndexes
     * @return array|mixed|null
     * @throws SessionWontStartException
     */
    public static function SESSION(...$deeperIndexes)
    {
        self::startSession();
        return Utils::valueFrom($_SESSION, ...$deeperIndexes);
    }

    /**
     * @param mixed ...$multipleIndexes
     * @return array
     */
    public static function multipleGET(...$multipleIndexes)
    {
        return Utils::valuesFrom($_GET, ...$multipleIndexes);
    }

    /**
     * @param mixed ...$multipleIndexes
     * @return array
     */
    public static function multiplePOST(...$multipleIndexes)
    {
        return Utils::valuesFrom($_POST, ...$multipleIndexes);
    }

    /**
     * @param mixed ...$multipleIndexes
     * @return array
     * @throws SessionWontStartException
     */
    public static function multipleSESSION(...$multipleIndexes)
    {
        self::startSession();
        return Utils::valuesFrom($_SESSION, ...$multipleIndexes);
    }

    /**
     * @param mixed ...$multipleIndexes
     * @return array
     * @throws InvalidCommandException
     */
    public static function requireGET(...$multipleIndexes)
    {
        return Utils::requiredValuesFrom($_GET, ...$multipleIndexes);
    }

    /**
     * @param mixed ...$multipleIndexes
     * @return array
     * @throws InvalidCommandException
     */
    public static function requirePOST(...$multipleIndexes)
    {
        return Utils::requiredValuesFrom($_POST, ...$multipleIndexes);
    }

    /**
     * @param mixed ...$multipleIndexes
     * @return array
     * @throws SessionWontStartException
     * @throws InvalidCommandException
     */
    public static function requireSESSION(...$multipleIndexes)
    {
        self::startSession();
        return Utils::requiredValuesFrom($_SESSION, ...$multipleIndexes);
    }
}