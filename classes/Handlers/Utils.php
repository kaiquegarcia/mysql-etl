<?php

namespace ETL\Handlers;


use ETL\Exceptions\InvalidCommandException;

class Utils
{
    const REQUIRED_ERROR = "Comando inválido.";

    /**
     * @param array $haystack
     * @param mixed ...$deeperIndexes
     * @return array|mixed|null
     */
    public static function valueFrom(array $haystack, ...$deeperIndexes)
    {
        if (empty($deeperIndexes)) {
            return $haystack;
        }
        foreach ($deeperIndexes as $index) {
            if (isset($haystack[$index])) {
                $haystack = $haystack[$index];
            } else {
                $haystack = null;
                break;
            }
        }
        return $haystack;
    }

    /**
     * @param array $haystack
     * @param mixed ...$multipleIndexes
     * @return array
     */
    public static function valuesFrom(array $haystack, ...$multipleIndexes)
    {
        $output = [];
        if (empty($multipleIndexes)) {
            return $output;
        }
        foreach ($multipleIndexes as $index) {
            $output[$index] = self::valueFrom($haystack, $index);
        }
        return $output;
    }

    /**
     * @param array $haystack
     * @param mixed ...$multipleIndexes
     * @return array
     * @throws InvalidCommandException
     */
    public static function requiredValuesFrom(array $haystack, ...$multipleIndexes)
    {
        $output = [];
        if (empty($multipleIndexes)) {
            return $output;
        }
        foreach ($multipleIndexes as $index) {
            $output[$index] = self::valueFrom($haystack, $index);
            if ($output[$index] === null) {
                throw new InvalidCommandException("$index está nulo.");
            }
        }
        return $output;
    }

    /**
     * @param array $haystack
     * @param string $filename
     * @return null
     */
    public static function toCSV(array &$haystack, $filename = "export")
    {
        if (empty($haystack)) {
            return null;
        }
        $filename .= "_" . date('d.m.Y_H.i') . '.csv';
        $now = gmdate("D, d M Y H:i:s");
        header("Expires: $now GMT");
        header("Cache-Control: max-age=0, no-cache, must-revalidate, proxy-revalidate");
        header("Last-Modified: {$now} GMT");
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/csv");
        header("Content-Disposition: attachment;filename=$filename");
        header("Content-Transfer-Encoding: binary");
        $df = fopen('php://output', 'w');
        fputs($df, $bom = (chr(0xEF) . chr(0xBB) . chr(0xBF)));
        fputcsv($df, array_keys(reset($haystack)), ';');
        foreach ($haystack as $row) {
            fputcsv($df, $row, ';');
        }
        fclose($df);
        exit();
    }


    public static function toURL($string)
    {
        $pattern_a = 'ÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖØÙÚÛÜüÝÞßàáâãäåæçèéêëìíîïðñòóôõöøùúûýýþÿŔŕ"!@#$%&*()_-+={[}]/?;:.,\\\'<>°ºª';
        $pattern_b = 'aaaaaaaceeeeiiiidnoooooouuuuuybsaaaaaaaceeeeiiiidnoooooouuuyybyRr                                 ';

        return strtolower(
            utf8_encode(
                str_replace(
                    ["-----", "----", "---", "--"],
                    "-",
                    str_replace(
                        " ",
                        "-",
                        strip_tags(trim(strtr(utf8_decode($string), utf8_decode($pattern_a), $pattern_b)))
                    )
                )
            )
        );
    }
}