<?php

namespace ETL\Handlers;

use ETL\Database\Log;
use ETL\Exceptions\InvalidCommandException;

class InterpreterOperation {
    private $method = null,
            $left,
            $right;
    private $log;

    public function __construct(Log $interpreterLog)
    {
        $this->log = $interpreterLog;
        $this->log->addInfo("Instancing a new InterpreterOperation");
        $this->log->addInfo("This default 'method' will call '_exec'");
    }
    public function setMethod($method)
    {
        $this->log->addInfo("Setting InterpreterOperation.method='_$method'");
        $this->method = $method;
    }

    public function setLeft($left)
    {
        $this->log->addInfo("Changing left chain");
        $this->left = $left;
    }

    public function setRight($right)
    {
        $this->log->addInfo("Changing right chain");
        $this->right = $right;
    }

    private function getValueOrResult($leftOrRight) {
        $this->log->addInfo("Starting getValueOrResult");
        if(is_callable($leftOrRight)) {
            $this->log->addInfo("Its a callable function. Lets call it");
            return $leftOrRight();
        } elseif($leftOrRight instanceof self) {
            $this->log->addInfo("Its an instance of InterpreterOperation. Lets call its call method");
            return $leftOrRight->call();
        } else {
            $this->log->addInfo("Its a value. Lets return it");
            return $leftOrRight;
        }
    }

    private function getLeft()
    {
        $this->log->addInfo("Getting left valueOrResult");
        return $this->getValueOrResult($this->left);
    }

    private function getRight()
    {
        $this->log->addInfo("Getting right valueOrResult");
        return $this->getValueOrResult($this->right);
    }

    public function call()
    {
        $this->log->addInfo("Calling the InterpreterOpration");
        if($this->method == null) {
            $this->log->addInfo("Its a main execution");
            return $this->_exec();
        } elseif(is_callable($this->method)) {
            $this->log->addInfo("Its a user function. Lets call it with left and right as arguments");
            return call_user_func($this->method, $this->getLeft(), $this->getRight());
        } elseif(method_exists($this, $this->method)) {
            $this->log->addInfo("Its one of our methods. Calling this->{$this->method}()");
            return $this->{$this->method}();
        }
        $this->log->addError("Invalid method {$this->method}");
        return null;
    }

    public function isCallable()
    {
        return $this->method != null || $this->left != null;
    }

    protected function _or($a)
    {
        return $this->getLeft() || $this->getRight();
    }
    protected function _and()
    {
        return $this->getLeft() && $this->getRight();
    }
    protected function _sum()
    {
        return $this->getLeft() + $this->getRight();
    }
    protected function _sub()
    {
        return $this->getLeft() - $this->getRight();
    }
    protected function _mul()
    {
        return $this->getLeft() * $this->getRight();
    }

    /**
     * @return float|int
     * @throws InvalidCommandException
     */
    protected function _div() {
        $right = $this->getRight();
        if($right === 0) {
            $this->log->addError("Division by zero");
            throw new InvalidCommandException("Divisão por zero.");
        }
        return $this->getLeft()/$right;
    }
    protected function _exec()
    {
        return $this->getValueOrResult($this->getLeft());
    }
}