<?php

namespace ETL\Handlers;

use ETL\Database\Log;

class Interpreter {
    const CONCATE = ",";
    const OPERATIONS = [
        "%" => "_or",
        "^" => "_and",
        "+" => "_sum",
        "-" => "_sub",
        "*" => "_mul",
        "/" => "_div",
    ];

    private $phrases;
    private $result;
    private $input;
    private $log;
    public function __construct(array $input, $phrase)
    {
        $this->log = new Log("interpreter.log");

        $this->log->addInfo("Instancing Interpreter");

        $this->setInput($input);
        $this->setPhrases($phrase);
        $this->interpret();
    }

    private function setInput($input)
    {
        $this->log->addInfo("Setting input");
        $this->log->addJSON($input);
        $this->input = $input;
    }

    private function setPhrases($phrase)
    {
        $this->log->addInfo("Setting phrase '$phrase'");
        $this->phrases = explode(self::CONCATE, $phrase);
    }

    private function interpret()
    {
        $this->log->addInfo("Starting interpretation loop");
        $result = [];
        foreach($this->phrases as $phrase) {
            if($phrase !== "") {
                $this->log->addInfo("Found a not empty phrase '$phrase'. Starting call->(phrase)");
                $caller = $this->call($phrase);
                if ($caller instanceof InterpreterOperation) {
                    $this->log->addInfo("Result of call is an instance of InterpreterOperation. Starting caller->call()");
                    $result[] = $caller->call();
                } else {
                    $this->log->addInfo("Result of call is the value '$caller'");
                    $result[] = $caller;
                }
            }
        }
        $this->log->addInfo("Ending interpretation loop");
        $this->log->addJSON($result);
        $result = implode("", $result);
        if($result != "") {
            if (!preg_match("/[^0-9]/", $result)) {
                $this->log->addInfo("The result is an integer");
                $result = intval($result);
            } elseif (!preg_match("/[^0-9.]/", $result)) {
                $this->log->addInfo("The result is a float number");
                $result = floatval($result);
            } else {
                $this->log->addInfo("The result has mixed type");
            }
        } else {
            $this->log->addInfo("Empty result");
        }
        $this->result = $result;
    }

    private function getColumnOrValue($data)
    {
        $this->log->addInfo("Getting Column or Input Value");
        if(is_string($data) && isset($this->input[$data])) {
            $this->log->addInfo("Its a input value input.$data={$this->input[$data]}");
            return $this->input[$data];
        } else {
            $this->log->addInfo("Its just a value '$data'");
            return $data;
        }
    }

    private function call($phrase)
    {
        $this->log->addInfo("Starting call phrase '$phrase'");
        $this->log->addInfo("Instance a main InterpreterOperation of this call");
        $exec = new InterpreterOperation($this->log);
        $this->log->addInfo("Iterating over self::OPERATIONS");
        foreach(self::OPERATIONS as $code => $method) {
            if(strpos($phrase, $code) !== false) {
                $this->log->addInfo("Operation '$code' found. Querying");
                list($left, $right) = explode($code, $phrase, 2);
                $this->log->addInfo("Exploded. Left:");
                $this->log->addJSON($left);
                $this->log->addInfo("Right:");
                $this->log->addJSON($right);
                $this->log->addInfo("Instancing a new InterpreterOperation");
                $operation = new InterpreterOperation($this->log);
                $operation->setMethod($method);
                $operation->setLeft(
                    $this->call($left)
                );
                $operation->setRight(
                    $this->call($right)
                );
                $this->log->addInfo("Setting operation as main left chain");
                $exec->setLeft($operation);
                break;
            }
        }
        $this->log->addInfo("self::OPERATIONS loop ended.");
        if($exec->isCallable()) {
            $this->log->addInfo("This main InterpreterOperation is callable. Lets return it");
            return $exec;
        } else {
            $this->log->addInfo("This main InterpreterOperation isnt callable");
            return $this->getColumnOrValue($phrase);
        }
    }

    public function getResult()
    {
        return $this->result;
    }
}