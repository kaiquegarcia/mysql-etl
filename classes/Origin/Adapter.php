<?php

namespace ETL\Origin;

use ETL\Database\Repository;
use ETL\Exceptions\DatabaseException;
use ETL\Exceptions\InvalidCommandException;
use ETL\Origin\Column;

class Adapter
{
    private $columns = [];

    /**
     * Adapter constructor.
     * @param array $ETL
     */
    public function __construct(array $ETL)
    {
        $rows = count($ETL['inputNames']);
        for ($i = 0; $i < $rows; $i++) {
            $this->columns[] = new Column(
                $ETL['outputNames'][$i],
                $ETL['inputNames'][$i],
                $ETL['inputValues'][$i],
                $ETL['inputValuesTranslations'][$i],
                $ETL['inputValuesPregTranslations'][$i]
            );
        }
    }

    /**
     * @param array $input
     * @return array
     * @throws InvalidCommandException
     */
    public function getOutput(array $input)
    {
        if (empty($this->columns)) {
            throw new InvalidCommandException("Não há colunas para converter!");
        }
        $output = [];
        foreach ($this->columns as $column) {
            $output = array_merge($output, $column->getOutput($input));
        }
        return $output;
    }

    /**
     * @param Repository $targetRepo
     * @param array $input
     * @return bool|mixed
     * @throws InvalidCommandException
     * @throws DatabaseException
     */
    public function transferTo(
        Repository $targetRepo,
        array $input
    )
    {
        $targetRepo->insert(
            $this->getOutput($input)
        );
    }

    /**
     * @param Repository $targetRepo
     * @param array $input
     * @return bool|mixed
     * @throws DatabaseException
     * @throws InvalidCommandException
     */
    public function prepareInsert(
        Repository $targetRepo,
        array $input
    )
    {
        $targetRepo->prepareSingleInsert(
            $this->getOutput($input)
        );
    }

}