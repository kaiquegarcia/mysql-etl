<?php

namespace ETL\Origin;

use ETL\ForceUTF8\Encoding;
use ETL\Handlers\Interpreter;
use ETL\Handlers\Utils;

class Column
{
    private $inputName;
    private $inputValues;
    private $inputValuesTranslations;
    private $inputValuesPregTranslations;
    private $outputName;

    /**
     * Column constructor.
     * @param $outputName
     * @param null $inputName
     * @param string $inputValues
     * @param string $inputValuesTranslations
     * @param string $inputValuesPregTranslations
     */
    public function __construct(
        $outputName,
        $inputName = null,
        $inputValues = "",
        $inputValuesTranslations = "",
        $inputValuesPregTranslations = ""
    )
    {
        $this->setOutputName($outputName);
        $this->setInputName($inputName);
        $this->setInputValues($inputValues);
        $this->setInputValuesTranslations($inputValuesTranslations);
        $this->setInputValuesPregTranslations($inputValuesPregTranslations);
    }

    /**
     * @param $input
     * @return string
     */
    private function generateValue($input)
    {
        $interpreter = new Interpreter($input, $this->inputName);
        return $interpreter->getResult();
    }

    /**
     * @param $value
     * @return mixed
     */
    private function translateValue($value)
    {
        if (!empty($this->inputValuesTranslations) && isset($this->inputValuesTranslations[$value])) {
            $value = $this->inputValuesTranslations[$value];
        }
        return $value;
    }

    /**
     * @param $value
     * @return string|string[]|null
     */
    private function translateRegExpressions($value)
    {
        if(!empty($this->inputValuesPregTranslations)) {
            foreach($this->inputValuesPregTranslations as $regex => $replace) {
                $value = preg_replace($regex, $replace, $value);
            }
        }
        return $value;
    }

    /**
     * @param $input
     * @return string|string[]|null
     */
    private function getValueFromInputName($input)
    {
        $value = $this->generateValue($input);
        $value = $this->translateValue($value);
        return $this->translateRegExpressions($value);
    }

    /**
     * @return mixed
     */
    private function getOneOfValues()
    {
        $randomIndex = array_rand($this->inputValues);
        return $this->inputValues[$randomIndex];
    }

    /**
     * @param array $input
     * @return array
     */
    public function getOutput(array $input)
    {
        if ($this->inputName != null) {
            $value = $this->getValueFromInputName($input);
        } elseif (!empty($this->inputValues)) {
            $value = $this->getOneOfValues();
        } else {
            return [];
        }
        if(is_string($value)) {
            $value = Encoding::toUTF8($value);
            $value = Encoding::fixUTF8($value);
            if($this->outputName === "URL") {
                $value = Utils::toURL($value);
            }
        }
        return [$this->outputName => $value];
    }

    /**
     * @param string|null $inputName
     */
    public function setInputName($inputName = null)
    {
        $this->inputName = $inputName;
    }

    /**
     * @param string $inputValues
     */
    public function setInputValues($inputValues = "")
    {
        $this->inputValues = [];
        $inputValues = preg_split('/\r\n|[\r\n]/', $inputValues);
        foreach ($inputValues as $value) {
            if ($value != null) {
                $this->inputValues[] = $value;
            }
        }
    }

    /**
     * @param string $inputValuesTranslations
     */
    public function setInputValuesTranslations($inputValuesTranslations = "")
    {
        $this->inputValuesTranslations = [];
        $inputValuesTranslations = preg_split('/\r\n|[\r\n]/', $inputValuesTranslations);
        foreach ($inputValuesTranslations as $row) {
            $rule = explode("=", $row);
            if (count($rule) == 2 && $rule[1] != null) {
                $this->inputValuesTranslations[$rule[0]] = $rule[1];
            }
        }
    }

    /**
     * @param string $inputValuesPregTranslations
     */
    public function setInputValuesPregTranslations($inputValuesPregTranslations = "")
    {
        $this->inputValuesPregTranslations = [];
        $inputValuesPregTranslations = preg_split('/\r\n|[\r\n]/', $inputValuesPregTranslations);
        foreach ($inputValuesPregTranslations as $row) {
            $rule = explode("=", $row);
            if (count($rule) == 2 && $rule[0] != null && $rule[1] != null) {
                $this->inputValuesPregTranslations[$rule[0]] = $rule[1];
            }
        }
    }

    /**
     * @param $outputName
     */
    public function setOutputName($outputName)
    {
        $this->outputName = $outputName;
    }
}